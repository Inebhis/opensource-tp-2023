# opensource-tp-2023

Salut la team,

Si vous suivez ce cours, veuillez rejoindre ce projet avec votre compte.

* Ayez un compte gitlab
* Rejoignez ce projet (request access)
* Puis une fois le projet fermé:
  * Créez une branche préfixé `STU-` à votre nom-prenom : (exemple STU-simon-alan pour alan simon)
  * Dans cette branche **votre dossier personnel** à votre 'nom-prenom' dans le dossier students.
  * Dans ce dossier un fichier me.txt de la forme :

```ascii
student : Nom Prenom
email : votre adresse mail
vous professionelement : 
 - tout ce qui est intéressant sur votre situation pro vos aspiration et vos attente
 - réalisation dons vous etes fier
 - votre next dans la liste des techno à voir 
evaluation du cour :
  toutes vos remarques sur le cours sont ici les bienvenues, une a minima...
```

* Vous placerez dans ce dossier vos travaux
* Vous pousserez votre branche sur le dépot git du cours régulièrement

## rendus

Un fichier README.md sera présent à la racine **de votre dossier personnel**
contenant un lien vers : 
* Vos rendu de TP
* Votre projet d'étude

## Règles 

### tenu de votre branche

Le merge automatique doit être possible. 
* Votre dossier personnel dans le dossier students
* Pas de modification à l’extérieur de votre dossier personnel
* Vous devez re-merger master avant de pousser votre branche

### travaux a plusieurs

Si vous avez travailler à plusieurs sur un document : 
 Assurez vous de ne pas travailler à plus de 4
 Ajoutez les dans le premier chapitre de niveau 2 markdown du document  avec tout les contributeurs en commençant par le rédacteur principale, vous rendez alpors tous le même fichier.

```
## contributeurs

nom-prenom
nom2-prenom2
.../...
```
